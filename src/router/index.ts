import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
    {
      path: "/my-files-view",
      name: "My Files view",
      component: () => import("../views/MyFilesView.vue"),
    },
    {
      path: "/my-profile-view",
      name: "My profile",
      component: () => import("../views/MyProfile.vue"),
    },
    {
      path: "/starred-view",
      name: "Starred",
      component: () => import("../views/StarredView.vue"),
    },
  ],
});

export default router;
